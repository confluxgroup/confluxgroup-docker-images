#!/bin/bash
set -euo pipefail

php_version=()
php_version=( "${1:-}" ) 

if [[ $# == 0 ]]; then
    php_version=(  "8.2" "8.1" "8.0" "7.4" "7.3" "7.2" "7.1" "7.0" "5.6" )
fi 

# loop through each image tag and process
for i in "${!php_version[@]}"
do
    # build local image
    echo "*** Building confluxgroup/php-apache:${php_version[$i]}"
    docker build --no-cache -t cg-php-apache:${php_version[$i]} ./php-apache/${php_version[$i]}

    # tag images
    docker tag cg-php-apache:${php_version[$i]} confluxgroup/php-apache:${php_version[$i]}

    # push images
    echo "*** Pushing confluxgroup/php-apache:${php_version[$i]}"
    docker push confluxgroup/php-apache:${php_version[$i]}

done