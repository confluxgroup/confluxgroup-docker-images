#!/bin/bash

# Custom document root
if [ ! -z "$WEB_DOCUMENT_ROOT" ]
then
sed -i "s|\/var\/www\/html|$WEB_DOCUMENT_ROOT|g" /etc/apache2/sites-available/000-default.conf
fi

# Custom php.ini config
if [ ! -z "$PHP_MEMORY_LIMIT" ]
then
sed -i "s/memory_limit =.*/memory_limit = '$PHP_MEMORY_LIMIT'/" $PHP_INI_PATH/apache2/php.ini
sed -i "s/memory_limit =.*/memory_limit = '$PHP_MEMORY_LIMIT'/" $PHP_INI_PATH/cli/php.ini
fi

if [ ! -z "$PHP_UPLOAD_MAX_FILESIZE" ]
then
sed -i "s/upload_max_filesize =.*/upload_max_filesize = '$PHP_UPLOAD_MAX_FILESIZE'/" $PHP_INI_PATH/apache2/php.ini
sed -i "s/upload_max_filesize =.*/upload_max_filesize = '$PHP_UPLOAD_MAX_FILESIZE'/" $PHP_INI_PATH/cli/php.ini
fi

if [ ! -z "$PHP_POST_MAX_SIZE" ]
then
sed -i "s/post_max_size =.*/post_max_size = '$PHP_POST_MAX_SIZE'/" $PHP_INI_PATH/apache2/php.ini
sed -i "s/post_max_size =.*/post_max_size = '$PHP_POST_MAX_SIZE'/" $PHP_INI_PATH/cli/php.ini
fi

if [ ! -z "$PHP_MAX_INPUT_VARS" ]
then
sed -i "s/;max_input_vars =.*/max_input_vars = '$PHP_MAX_INPUT_VARS'/" $PHP_INI_PATH/apache2/php.ini
sed -i "s/;max_input_vars =.*/max_input_vars = '$PHP_MAX_INPUT_VARS'/" $PHP_INI_PATH/cli/php.ini
fi

if [ ! -z "$PHP_MAX_EXECUTION_TIME" ]
then
sed -i "s/max_execution_time =.*/max_execution_time = '$PHP_MAX_EXECUTION_TIME'/" $PHP_INI_PATH/apache2/php.ini
sed -i "s/max_execution_time =.*/max_execution_time = '$PHP_MAX_EXECUTION_TIME'/" $PHP_INI_PATH/cli/php.ini
fi