# PHP with Apache container

This image is based on [Phusion BaseImage](https://github.com/phusion/baseimage-docker), which is a minimal Ubuntu base image that allows for running multiple processes in a Docker-friendly way.

Container                               | PHP Version
--------------------------------------- | ------------------------
`confluxgroup/php-apache:7.0`      	    | PHP 7.0
`confluxgroup/php-apache:7.1`      		| PHP 7.1
`confluxgroup/php-apache:7.2`      		| PHP 7.2
`confluxgroup/php-apache:7.3`      		| PHP 7.3
`confluxgroup/php-apache:7.4`      		| PHP 7.4
`confluxgroup/php-apache:8.0`      		| PHP 8.0
`confluxgroup/php-apache:8.1`      		| PHP 8.1
`confluxgroup/php-apache:8.2`      		| PHP 8.2

## Environment variables

Variable                  | Description
------------------------- |  ------------------------------------------------------------------------------
`PUID`			          | Apache UID (Effective user ID)
`PGID`     			      | Apache GID (Effective group ID)
`WEB_DOCUMENT_ROOT`       | Document root for Nginx
`PHP_MEMORY_LIMIT`        | PHP Memory Limit
`PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time
`PHP_UPLOAD_MAX_FILESIZE` | PHP Upload Max Filesize
`PHP_POST_MAX_SIZE`       | PHP Post Max Size
`PHP_MAX_INPUT_VARS`      | PHP Max Input Vars

## Filesystem layout

Directory                       | Description
------------------------------- | ------------------------------------------------------------------------------
`/var/www/html`                 | Default Apache document root

File                                             | Description
------------------------------------------------ |--------------------------------------------------------------
`/etc/apache2/sites-available/000-default.conf`  | Default Apache configuration file
