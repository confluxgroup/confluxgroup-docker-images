server {
    listen 80 default_server;
    listen [::]:80 default_server;

    # General virtual host settings
    server_name _;
    root "/var/www/html";
    index index.html index.htm index.php;
    charset utf-8;

    # Disable limits on the maximum allowed size of the client request body
    client_max_body_size 0;

    # Enable serving of static gzip files as per: http://nginx.org/en/docs/http/ngx_http_gzip_static_module.html
    gzip_static  on;

    # 404 error handler
    error_page 404 /index.php?$query_string;

    # Don't send the nginx version number in error pages and Server header
    server_tokens off;

    # Root directory location handler
    location / {
        try_files $uri/index.html $uri $uri/ /index.php?$query_string;
    }

    # php-fpm configuration
    location ~ [^/]\.php(/|$) {
        try_files $uri $uri/ /index.php?$query_string;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_param SERVER_NAME $host;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param HTTP_PROXY "";

        # Don't allow browser caching of dynamically generated content
        add_header Last-Modified $date_gmt;
        add_header Cache-Control "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0";
        if_modified_since off;
        expires off;
        etag off;

        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    # Disable reading of Apache .htaccess files
    location ~ /\.ht {
        deny all;
    }

    # Misc settings
    sendfile off;

    # Load configuration files from include.d
    include /etc/nginx/include.d/*.conf;
}