# Dockerfile
FROM phusion/baseimage:0.11
ENV DEBIAN_FRONTEND noninteractive

ENV PHP_VERSION 7.2
ENV PHP_INI_PATH /etc/php/$PHP_VERSION

# Install PHP
RUN add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y \
        nano \
        git \
        zip \
        unzip \
        # Only install the php extensions you need.
        php$PHP_VERSION \
        php$PHP_VERSION-fpm \
        php$PHP_VERSION-cli \
        php$PHP_VERSION-imagick \
        php$PHP_VERSION-intl \
        php$PHP_VERSION-mbstring \
        php$PHP_VERSION-xml \
        php$PHP_VERSION-dom \
        php$PHP_VERSION-zip \
        php$PHP_VERSION-bcmath \
        php$PHP_VERSION-gmp \
        php$PHP_VERSION-mysql \
        php$PHP_VERSION-curl \
        php$PHP_VERSION-gd \
        php$PHP_VERSION-imap \
        php$PHP_VERSION-tidy \
        php$PHP_VERSION-iconv \
        nginx \
    && rm -rf /var/lib/apt/lists/*

# Create socket directory
RUN mkdir -p /var/run/php

# Don't clear env variables
# This is very important since it will allow us to read environment variables from the container.
RUN sed -e 's/;clear_env = no/clear_env = no/' -i /etc/php/$PHP_VERSION/fpm/pool.d/www.conf

ADD ./10-change-uid-gid.sh /etc/my_init.d/
RUN chmod +x /etc/my_init.d/10-change-uid-gid.sh

ADD ./20-runtime-configuration.sh  /etc/my_init.d/
RUN chmod +x /etc/my_init.d/20-runtime-configuration.sh

# create include.d directory for adding nginx conf includes inside the server block
RUN mkdir -p /etc/nginx/include.d

# set up default nginx server block
ADD ./site.conf /etc/nginx/sites-available/default

# Hook up stdout to nginx/php
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /dev/stdout /var/log/php$PHP_VERSION-fpm.log

# configure services to run in this container
RUN mkdir       /etc/service/nginx
ADD ./nginx.sh  /etc/service/nginx/run
RUN chmod +x    /etc/service/nginx/run

RUN mkdir       /etc/service/php
ADD ./php.sh    /etc/service/php/run
RUN chmod +x    /etc/service/php/run

# install composer
WORKDIR /tmp
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
RUN composer global require hirak/prestissimo

WORKDIR /var/www/
EXPOSE 80
CMD ["/sbin/my_init"]